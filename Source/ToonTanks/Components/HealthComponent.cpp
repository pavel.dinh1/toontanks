// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthComponent.h"
#include "../GameModes/TankGameModeBase.h"
#include "Kismet/GameplayStatics.h"

// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	bDebug = true;
}

// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	Health = DefaultHealth;
	if (bDebug) { UE_LOG(LogTemp, Warning, TEXT("%s CurrentHealth : %f"), *GetOwner()->GetName(), Health); }

	GameModeRef = Cast<ATankGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
	GetOwner()->OnTakeAnyDamage.AddDynamic(this, &UHealthComponent::TakeDamage);
}

void UHealthComponent::TakeDamage(AActor* DamageActor, float Damage, const UDamageType* DamageType, AController* InstigatorBy, AActor* DamageCauser)
{
	if (Damage == 0.f || Health == 0.f) { return; }

	Health = FMath::Clamp(Health - Damage, 0.f, DefaultHealth);
	if (bDebug) { UE_LOG(LogTemp, Warning, TEXT("%s CurrentHealth : %f"), *GetOwner()->GetName(), Health); }

	if (Health <= 0)
	{
		if (GameModeRef)
		{
			GameModeRef->ActorDied(GetOwner());
		}
		else
		{
			if (bDebug) { UE_LOG(LogTemp, Warning, TEXT("Health component has no reference to the GameMode")); }
		}
	}
}