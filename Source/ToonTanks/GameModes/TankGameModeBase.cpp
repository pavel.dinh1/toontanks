// Fill out your copyright notice in the Description page of Project Settings.


#include "TankGameModeBase.h"
#include "../Pawns/PawnTank.h"
#include "../Pawns/PawnTurret.h"
#include "../PlayerControllers/PlayerControllerBase.h"
#include "Kismet/GameplayStatics.h"

ATankGameModeBase::ATankGameModeBase()
{
	TargetTurrets = 0;
	StartDelay = 3;
}

void ATankGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	//TODO: game win/lose condition.

	HandleGameStart();
}

void ATankGameModeBase::ActorDied(AActor* DeadActor)
{
	// Check what type of Actor died. If turret, ally. If Player -> go to lose condition.
	// UE_LOG(LogTemp, Warning, TEXT("A Pawn Died !"));

	if (DeadActor == PlayerTank)
	{
		PlayerTank->HandleDestruction();
		HandleGameOver(false);

		if (PlayerControllerRef) { PlayerControllerRef->SetPlayerEnabledState(false); }
	}
	else if (APawnTurret* DestroyedTurret = Cast<APawnTurret>(DeadActor))
	{
		DestroyedTurret->HandleDestruction();
		if (--TargetTurrets == 0)
		{
			HandleGameOver(true);
		}
	}
}

// Initialize the start countdown, turret activation, pawn check etc.
void ATankGameModeBase::HandleGameStart()
{
	TargetTurrets = GetTargetTurretCount();
	PlayerTank = Cast<APawnTank>(UGameplayStatics::GetPlayerPawn(this, 0));
	PlayerControllerRef = Cast<APlayerControllerBase>(GetWorld()->GetFirstPlayerController());

	// Call Blueprint version GameStart().
	GameStart();
	if (PlayerControllerRef)
	{
		PlayerControllerRef->SetPlayerEnabledState(false);

		FTimerHandle PlayerEnableHandle;
		FTimerDelegate PlayerEnableDelegate = FTimerDelegate::CreateUObject
		(
			PlayerControllerRef,
			&APlayerControllerBase::SetPlayerEnabledState,
			true
		);

		GetWorld()->GetTimerManager().SetTimer(PlayerEnableHandle, PlayerEnableDelegate, StartDelay, false);
	}
}

void ATankGameModeBase::HandleGameOver(bool PlayerWon)
{
	// See if the player has destroyed all the turrets, show win result.
	// else if turret destroyed player, show lose result.
	// Call blueprint version GameOver(bool).
	GameOver(PlayerWon);
}

int32 ATankGameModeBase::GetTargetTurretCount()
{
	TArray<AActor*> TurretActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), APawnTurret::StaticClass(), TurretActors);
	return TurretActors.Num();
}